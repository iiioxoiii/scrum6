import java.util.*;


public class Aplicacion {

    public static void main(String[] args) {

        Aplicacion inici = new Aplicacion();
        inici.go();

    }


    public void go() {

        Scanner sc = new Scanner(System.in);

        boolean menu = true;

        //Instanciacio de la classe control on hi son els métodes del menu

        Controlador control= new Controlador();

        while (menu) {
            System.out.println("Elige el programa que quieres hacer:");
            System.out.println("1): Introducción de datos");
            System.out.println("2): Incidencias y tanto por ciento");
            System.out.println("3): Años del ordenador y calculo del presupuesto");
            System.out.println("4): Salir del programa");

            int numero = sc.nextInt();
            switch (numero) {
                case 1:
                    control.anadirPCs();
                    break;
                case 2:
                    control.incidencias();
                    break;
                case 3:
                    control.datos();
                    break;
                case 4:
                    menu = false;
                    break;
            }
        }
    }
}

