import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Incidencia {

    private ArrayList<Integer> myArray = new ArrayList<>();
    private int x = 1;

    private String ruta = "/home/21755008k/Baixades/Program/Presupuestos/incidencias";


    public void go() {

        Scanner sc = new Scanner(System.in);

        String solucionat;
        String fin;
        int totalincidencies;
        int resultat;
        int historial = 0;
        int operacio = 0;
        int solucio = 0;
        boolean exit = true;

        System.out.println("Quieres guardar todos los datos de las incidencias en un documento? (s/n)");
        String answer = sc.next().toLowerCase();
        int auxiliar = 0;

        if (answer.equals("s")) {
            String guiones = "\n---------------------------- \n";
            x++;
            escribirCadena(guiones);
            String inicio = "INCIDENCIAS " + x + ": \n";
            escribirCadena(inicio);
            auxiliar = 1;
        }

        while (exit) {

            int a = 0;

            System.out.println("Introduce el numero de incidencias: ");
            totalincidencies = sc.nextInt();

            String arrayi[] = new String[totalincidencies];

            for (int i = 0; i < arrayi.length; i++) {
                System.out.println("Ingresa la incidencia  posant 's' si s'ha solucionat i 'n' si no.");
                solucionat = sc.next();

                switch (solucionat) {
                    case "s":
                        a++;
                        arrayi[i] = "si";
                        break;
                    case "n":
                        arrayi[i] = "no";
                        break;
                    default:
                        System.out.println("Valor incorrecto" + "\n" + "Ingresa la incidencia  posant 's' si s'ha solucionat i 'n' si no.");
                        i--;
                        break;
                }

                if (solucionat.equals("s")) {
                    operacio = 1;
                } else if (solucionat.equals("n")) {
                    operacio = 0;
                }

                operacio = operacio + solucio;
            }

            resultat = (100 * a) / totalincidencies;

            System.out.println("Respostes " + Arrays.toString(arrayi));
            System.out.println("Es d'un " + resultat + "%");

            historyIncidencing(historial, resultat, "añadir", auxiliar);


            if (auxiliar == 1) {
                String answers2 = "Respostes " + Arrays.toString(arrayi);
                escribirCadena(answers2);
                String result = "Es d'un " + resultat + "% \n";
                escribirCadena(result);
            }


            System.out.println("Vols sortir? s = sortir");
            fin = sc.next();
            fin = fin.toLowerCase();
            if (fin.equals("s")) {
                historyIncidencing(historial, resultat, "mostrar", auxiliar);
                exit = false;
            }
        }

        System.out.println("Quieres ver el archivo con todas las incidencias hasta la fecha? (s/n)");
        String yn = sc.next().toLowerCase();

        if (yn.equals("s")) {
            leerLineas();
        }
    }




    private void historyIncidencing(int historial, int resultat, String opcion, int auxiliar) {

        switch (opcion) {
            case "mostrar":
                System.out.println("-----------------------\nHistorial");
                if (auxiliar == 1) {
                    String historiall = "\nHISTORIAL\n";
                    escribirCadena(historiall);
                }

                String historiall = "";
                System.out.print("[");
                for (int i = 0; i < this.myArray.size(); i++) {
                    System.out.print(this.myArray.get(i) + "%, ");
                    if (auxiliar == 1 && i >= 1) {
                        historiall = historiall + this.myArray.get(i) + "%, ";
                    }
                }
                escribirCadena(historiall);
                System.out.println("]");

                if (auxiliar == 1) {
                    String barras = "\n -----------------------";
                    escribirCadena(barras);
                }

                break;

            case "añadir":
                historial = resultat;
                this.myArray.add(historial);

                break;
        }
    }




    public void escribirCadena(String cadena) {

        try {

            File presupuestos = new File(ruta);

            System.out.println("S'ha creat l'arxiu: " + presupuestos.createNewFile());

            FileWriter fw = new FileWriter(ruta, true);

            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("\n" + cadena);
            bw.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Ha hagut un problema en la creació de l'arxiu");
        }
    }



    public void leerLineas() {

        try {

            System.out.println("Su ordenador esta siendo analizado...");

            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);

            String linia = br.readLine();

            while (linia != null) {
                linia = br.readLine();
                System.out.println(linia + "\n");
            }

            br.close();

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}

