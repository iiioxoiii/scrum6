package Incidencies;
import java.util.Scanner;

public class Incidencia {

    public String opcio;
    private String estado;
    private String gravedad;
    private String fecha_creacion;
    private String fecha_solucionado;
    private int id_Incidencia;

    public Incidencia(String estado, String gravedad, String fecha_creacion, int id_incidencia) {
        this.estado = estado;
        this.gravedad = gravedad;
        this.fecha_creacion = fecha_creacion;
        id_Incidencia = id_incidencia;
    }


    public Incidencia() {
    }

// crear metodo incidencia (solucionado, proceso, emitido
    // if incidencia = solucionada , call fecha solucionado (poner valor)

    public void cambiarEstado(){
        Scanner sc = new Scanner(System.in);
        System.out.println("El estado de la incidencia "+id_Incidencia+" es "+estado);
        System.out.println("Puedes cambiar el estado a:\n1)Emitido\n2)Procesando\n3)Finalizada\n4)No cambiar");
        opcio = sc.next();
        switch(opcio) {
            case "1" :
                this.estado = "Emitida";
                System.out.println("Cambiado a "+this.estado);
                break;
            case "2" :
                this.estado = "Procesando";
                System.out.println("Cambiado a "+this.estado);
                break;
            case "3" :
                this.estado = "Finalizada";
                System.out.println("Cambiado a "+this.estado);
                break;
            case "4" :
                break;
            default :
                System.out.println("Invalid");
        }

        if(estado=="Finalizado"){
            System.out.println("La incidencia esta solucionada, digam la data de finalització.");
            this.fecha_solucionado = sc.next();
        }

    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getFecha_solucionado() {
        return fecha_solucionado;
    }

    public void setFecha_solucionado(String fecha_solucionado) {
        this.fecha_solucionado = fecha_solucionado;
    }

    public int getId_Incidencia() {
        return id_Incidencia;
    }

    public void setId_Incidencia(int id_Incidencia) {
        this.id_Incidencia = id_Incidencia;
    }
}
