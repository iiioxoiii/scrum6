package Incidencies;
import java.util.Scanner;
public class IncidenciaTest {

    public static void main(String[] args) {

        Incidencia inc1 = new Incidencia();
        Scanner sc = new Scanner(System.in);

        System.out.println("Benvigut al servei d'incidencies\nVols: 1)Crear incidencia 2)Modificar incidencia\3)Sortir");
        String opcio;

        opcio = sc.next();
        switch(opcio) {
            case "1" :
                System.out.println("Creació d'incidencies");
                break;
            case "2" :
                System.out.println("Modificació d'incidencies");
                inc1.cambiarEstado();
                break;
            case "3" :
                System.out.println("Sortir");

                break;
            default :
                System.out.println("Invalid");
        }
    }
}