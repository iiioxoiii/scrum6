import java.util.ArrayList;

public interface Inventario {

    public void setCodPC(String _codPC);

    public void setAulaPC(String _aulaPC);

    public void setCpuPC(String _cpuPC);

    public void setRamPC(int _ramPC);

    public void setAnoCompraPC(int _anoCompraPC);
    

    public ArrayList<String> getCodPC();

    public ArrayList<String> getAulaPC();

    public ArrayList<String> getCpuPC();

    public ArrayList<Integer> getRamPC();

    public ArrayList<Integer> getAnoCompraPC();


    public void corregirDatosPCs();

    public void corregir1PC(int numPC);

    public void corregirVariosPC();

    public void mostrarPCs();

    public void guardarDatos();

    public void anadirPCs();

}
