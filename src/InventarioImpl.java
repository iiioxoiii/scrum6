import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;


class InventarioImpl implements Inventario{


    // ATRIBUTOS

    // ARCHIVO DONDE SE ALMACENARAN LOS DATOS
    private File inventario;
    private int numPC = 0;


    private ArrayList<String> codPC = new ArrayList<>(); // Codigo del PC
    private ArrayList<String> aulaPC = new ArrayList<>(); // Aula donde se encuentra el PC
    private ArrayList<String> cpuPC = new ArrayList<>(); // Modelo de procesador del PC
    private ArrayList<Integer> ramPC = new ArrayList<>(); // Cantidad de memoria RAM del PC
    private ArrayList<Integer> anoCompraPC = new ArrayList<>(); // Año de compra del PC


    public InventarioImpl() {
    }



    // SETTERS
    public void setCodPC(String _codPC) {
        codPC.add(_codPC);
    }
    public void setAulaPC(String _aulaPC) {
        aulaPC.add(_aulaPC);
    }
    public void setCpuPC(String _cpuPC) {
        cpuPC.add(_cpuPC);
    }
    public void setRamPC(int _ramPC) {
        ramPC.add(_ramPC);
    }
    public void setAnoCompraPC(int _anoCompraPC) {
        anoCompraPC.add(_anoCompraPC);
    }


    // GETTERS
    public ArrayList<String> getCodPC() {
        return codPC;
    }
    public ArrayList<String> getAulaPC() {
        return aulaPC;
    }
    public ArrayList<String> getCpuPC() {
        return cpuPC;
    }
    public ArrayList<Integer> getRamPC() {
        return ramPC;
    }
    public ArrayList<Integer> getAnoCompraPC() {
        return anoCompraPC;
    }



    public void go(){

        Scanner sc = new Scanner(System.in);

        System.out.println("Indica la ruta donde quieres almacenar los datos:\n  *Indica la ruta completa del archivo");
        String ruta = sc.next();

        try {

            inventario = new File(ruta);

        } catch (Exception e) {

            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * Añadir ordenadores al inventario
     */
    public void anadirPCs() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Cuantos PCs quieres añadir? ");
        int numPCs = sc.nextInt();

        System.out.println("#################################");
        System.out.println("#    Registro de ordenadores    #");
        System.out.println("#################################" + "\n");

        for (int y = 0; y < numPCs; y++) {
            System.out.println("  **** Ordinador " + numPCs + " ****");
            System.out.println("Introdueix el codi de l'ordinador: ");

            String cod = sc.next();
            setCodPC(cod);

            System.out.println("Aula on es troba el ordinador: ");
            String aula = sc.next();
            setAulaPC(aula);

            System.out.println("Model de microprocesador: ");
            String cpu = sc.next();
            setCpuPC(cpu);

            System.out.println("Quantitat de memoria RAM: ");
            int mem = sc.nextInt();
            setRamPC(mem);

            System.out.println("Any de compra: ");
            int ano = sc.nextInt();
            setAnoCompraPC(ano);
            System.out.println();
        }

        mostrarPCs();

        corregirDatosPCs();

        guardarDatos();

    }

    public void corregirDatosPCs() {
        Scanner sc = new Scanner(System.in);
        System.out.println();
        String correcto;
        boolean coreccion = true;
        int numPCcorregir = 0;
        while (coreccion) {
            System.out.println("Los datos introducidos son correctos? [si-no]");
            correcto = sc.next();
            switch (correcto) {
                case "si":
                    coreccion = false;
                    break;

                case "no":
                    while (true) {
                        System.out.println("Cuantos datos tienes que corregir?");
                        System.out.println("   1 - Un solo PC");
                        System.out.println("   2 - Mas de un PC");
                        numPCcorregir = sc.nextInt();

                        if (numPCcorregir == 1) {
                            System.out.println("Que numero de ordenador quieres editar?");
                            int numPC = sc.nextInt();

                            corregir1PC(numPC);
                            break;

                        } else if (numPCcorregir == 2) {
                            corregirVariosPC();
                            break;
                        }
                    }
                    coreccion = false;
                    break;

                default:
                    System.out.println("Opción incorrecta!");
                    break;
            }
        }
    }

    public void corregir1PC(int numPC) {
        int opcion;
        boolean menu = true;

        Scanner sc = new Scanner(System.in);


        while (menu) {
            System.out.println("Que quieres editar?");
            System.out.println("   0 - Identificador del PC");
            System.out.println("   1 - Aula donde se encuentra");
            System.out.println("   2 - Modelo de la CPU");
            System.out.println("   3 - Cantidad de memoria RAM");
            System.out.println("   4 - Año de compra");
            System.out.println("   5 - Salir de la edicion");
            opcion = sc.nextInt();
            switch (opcion) {
                case 0:
                    System.out.println("Introduce el nuevo identificador:");
                    codPC.set(numPC, sc.next());
                    break;

                case 1:
                    System.out.println("Introduce la nueva aula:");
                    aulaPC.set(numPC, sc.next());
                    break;

                case 2:
                    System.out.println("Introduce el nuevo modelo de CPU:");
                    cpuPC.set(numPC, sc.next());
                    break;

                case 3:
                    System.out.println("Introduce la nueva cantidad de memoria RAM:");
                    ramPC.set(numPC, sc.nextInt());
                    break;

                case 4:
                    System.out.println("Introduce el nuevo año de compra:");
                    anoCompraPC.set(numPC, sc.nextInt());
                    break;

                case 5:
                    menu = false;
                    break;

                default:
                    System.out.println("Opción incorrecta!");
                    break;
            }
        }
    }

    public void corregirVariosPC() {
        int numPC;
        boolean menu = true;

        Scanner sc = new Scanner(System.in);


        do {
            System.out.println("Introduce el numero de ordenador:");
            numPC = sc.nextInt();

            corregir1PC(numPC);

            System.out.println("Quieres modificar los datos de mas PCs? [si-no]");
            String corregir = sc.next();

            switch (corregir) {
                case "si":
                    break;

                case "no":
                    menu = false;
                    break;

                default:
                    System.out.println("Opción incorrecta");
                    break;
            }

        }

        while (menu);

    }

    public void mostrarPCs() {

        try {
            BufferedReader br = new BufferedReader(new FileReader(inventario));


            System.out.println("###############################");
            System.out.println("#   ORDENADORES REGISTRADOS   #");
            System.out.println("###############################");
            System.out.println();

            String linea;

            while((linea = br.readLine())!=null) {
                System.out.println(linea);
            }

        } catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void borrarPCs() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Cuantos ordenadores quieres borrar?");
        int numPCsBorrar = sc.nextInt();

        while (numPCsBorrar > 0) {
            System.out.println("Codigo del PC:");
            String codigo = sc.nextLine();

            int indice = codPC.indexOf(codigo);

            codPC.remove(indice);
            aulaPC.remove(indice);
            cpuPC.remove(indice);
            ramPC.remove(indice);
            anoCompraPC.remove(indice);

        }
    }

    public void guardarDatos(){
        boolean ok = false;

        if (!inventario.exists()){
            try {
                ok = inventario.createNewFile();
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }

            if (ok){
                System.out.println("El fichero de inventario se ha creado correctamente");
            } else {
                System.out.println("El fichero de inventario no existe ni se puede crear");
            }
        }

        try {

            BufferedWriter bw = new BufferedWriter(new FileWriter(inventario));

            for (int i = 0; i < codPC.size(); i++) {
                bw.write("***  Ordenador " + i + "  ***");
                bw.newLine();
                bw.write(" · Codigo ordenador: " + codPC.get(i));
                bw.newLine();
                bw.write(" · Aula: " + aulaPC.get(i));
                bw.newLine();
                bw.write(" · Modelo CPU: " + cpuPC.get(i));
                bw.newLine();
                bw.write(" · Memoria RAM: " + ramPC.get(i));
                bw.newLine();
                bw.write(" · Año: " + codPC.get(i));
                bw.newLine();
            }

            bw.close();
        } catch (Exception e) {

            System.out.println("Error: " + e.getMessage());
        }

    }
}
