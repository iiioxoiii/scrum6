public abstract class  Ordenador {

    private String id;
    private String aula;
    private String cpu;
    private Integer ram;
    private Integer ano;
    /*private boolean tieneIncidencias = false;
    private Incidencia incidencia;*/


    public Ordenador() {

    }


    public Ordenador(String id, String aula, String cpu, int ram, int ano) {
        this.id = id;
        this.aula = aula;
        this.cpu = cpu;
        this.ram = ram;
        this.ano = ano;
    }

    // GETTERS
    public String getId() {
        return id;
    }
    public String getAula() {
        return aula;
    }
    public String getCpu() {
        return cpu;
    }
    public Integer getRam() {
        return ram;
    }
    public Integer getAno() {
        return ano;
    }


    // SETTERS
    public void setId(String id) {
        this.id = id;
    }
    public void setAula(String aula) {
        this.aula = aula;
    }
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }
    public void setRam(Integer ram) {
        this.ram = ram;
    }
    public void setAno(Integer ano) {
        this.ano = ano;
    }


    @Override
    public String toString() {
        return "Ordenador{" +
                "id='" + id + '\'' +
                ", aula='" + aula + '\'' +
                ", cpu='" + cpu + '\'' +
                ", ram=" + ram +
                ", ano=" + ano +
                '}';
    }
}