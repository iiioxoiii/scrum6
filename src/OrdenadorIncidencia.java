public class OrdenadorIncidencia extends Ordenador {
    private boolean tieneIncidencia;

    public OrdenadorIncidencia(boolean tieneIncidencia) {
        this.tieneIncidencia = tieneIncidencia;
    }

    public OrdenadorIncidencia(String id, String aula, String cpu, int ram, int ano, boolean tieneIncidencia) {
        super(id, aula, cpu, ram, ano);
        this.tieneIncidencia = tieneIncidencia;
    }

    public boolean isTieneIncidencia() {
        return tieneIncidencia;
    }

    public void setTieneIncidencia(boolean tieneIncidencia) {
        this.tieneIncidencia = tieneIncidencia;
    }
}
