public class OrdenadorP extends Ordenador {

    private int precio;

    public OrdenadorP(String id, String aula, String cpu, Integer ram, Integer ano, Integer precio) {

        super(id, aula, cpu, ram, ano);
        this.precio = precio;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
