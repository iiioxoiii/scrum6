import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pc {

    private String codi;
    private String aula;
    private String cpu;
    private Integer ram;
    private Date anyCompra;


    public Pc(){
    }

    //Metode constructor amb any de compra en format String
    public Pc(String codi, String aula, String cpu, Integer ram, String anyCompra) {
        this.codi = codi;
        this.aula = aula;
        this.cpu = cpu;
        this.ram = ram;
        setAny(anyCompra);
    }

    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public Date getAny(){
        return anyCompra;
    }

    //Entrada amb String
    public void setAny(String any) {

        DateFormat df = new SimpleDateFormat("yyyy");

        try {
            this.anyCompra = df.parse(any);
        }catch (ParseException e){
            System.out.println("Entrada data malament");
        }
    }

    @Override
    public String toString() {

        DateFormat df = new SimpleDateFormat("yyyy");
        String dataCompra = df.format(anyCompra);

        return "Pc{" +
                "codi='" + codi + '\'' +
                ", aula='" + aula + '\'' +
                ", cpu='" + cpu + '\'' +
                ", ram=" + ram +
                ", data de compra=" + dataCompra +
                '}';
    }
}